package ru.geekbrains;

public class Main {
    /**
     * Задача 1
     * Необходимо написать два метода, которые делают следующее:
     * 1) Создают одномерный длинный массив, например:
     *    static final int size = 10000000;
     *    static final int h = size / 2;
     *    float[] arr = new float[size];
     * 2) Заполняют этот массив единицами;
     * 3) Засекают время выполнения: long a = System.currentTimeMillis();
     * 4) Проходят по всему массиву и для каждой ячейки считают новое значение по формуле:
     *    arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
     * 5) Проверяется время окончания метода System.currentTimeMillis();
     * 6) В консоль выводится время работы: System.out.println(System.currentTimeMillis() - a);
     *
     * Отличие первого метода от второго:
     * Первый просто бежит по массиву и вычисляет значения.
     * Второй разбивает массив на два массива, в двух потоках высчитывает новые значения и потом склеивает
     * эти массивы обратно в один.
     *
     * Пример деления одного массива на два:
     * System.arraycopy(arr, 0, a1, 0, h);
     * System.arraycopy(arr, h, a2, 0, h);
     *
     * Пример обратной склейки:
     * System.arraycopy(a1, 0, arr, 0, h);
     * System.arraycopy(a2, 0, arr, h, h);
     *
     * Примечание:
     * System.arraycopy() – копирует данные из одного массива в другой:
     * System.arraycopy(
     *      массив-источник,
     *      откуда начинаем брать данные из массива-источника,
     *      массив-назначение,
     *      откуда начинаем записывать данные в массив-назначение,
     *      сколько ячеек копируем
     * )
     *
     * По замерам времени:
     * Для первого метода надо считать время только на цикл расчета:
     * for (int i = 0; i < size; i++) {
     *      arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
     * }
     *
     * Для второго метода замеряете время разбивки массива на 2, просчета каждого из двух массивов и склейки.
     */
    public static void main(String[] args) throws InterruptedException {
        /**
         * Примечание к решению:
         * Вместо создания двух методов я позволил себе реализовать один метод который может выполнять обработку данных
         * в заданное количество потоков. После чего произвел сравнение времени выполнения манипуляций с массивом
         * в один и несколько потоков.
         */
        final int arrSize = 10000000;
        fillArray(arrSize, 1);
        fillArray(arrSize, 5);
    }

    private static void fillArray(int arrSize, int threadsCount) throws InterruptedException {
        if (threadsCount > arrSize)
            throw new IllegalArgumentException("Количество потоков не может превышать количество элементов в массиве!");
        float[] arr = new float[arrSize];
        for (int i = 0; i < arr.length; i++) arr[i] = 1;
        final int arrPartSize = arrSize / threadsCount;
        final int remainder = arrSize % threadsCount;
        final long startTime = System.currentTimeMillis();
        Thread[] threads = new Thread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            final int index = i;
            final int startCopy = i * arrPartSize;
            threads[i] = new Thread() {
                public void run() {
                    float[] arrPart;
                    if (index == threadsCount - 1 && remainder != 0) arrPart = new float[arrPartSize + remainder];
                    else arrPart = new float[arrPartSize];
                    System.arraycopy(arr, startCopy, arrPart, 0, arrPart.length);
                    calculate(arrPart, startCopy);
                    System.arraycopy(arrPart, 0, arr, startCopy, arrPart.length);
                }
            };
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i++) threads[i].join();
        final long execTime = System.currentTimeMillis() - startTime;
        System.out.println("Потоков - " + threadsCount + ". Время выполнения: " + execTime);
    }

    private static void calculate(float[] arr, int j) {
        for (int i = 0; i < arr.length; i++, j++) {
            arr[i] = (float)(arr[i] * Math.sin(0.2f + j / 5) * Math.cos(0.2f + j / 5) * Math.cos(0.4f + j / 2));
        }
    }
}
